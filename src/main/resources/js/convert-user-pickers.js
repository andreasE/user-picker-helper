(function ($) {

    var restPath = "/rest/api/1.0/users/picker";

    function setFieldDescription($field, inputText) {
        if (!!inputText) {

            var usersFound = [];
            var deferreds = [];
            inputText.split(/\s*,/).forEach(function (username) {

                if (/\w+/.test(username)) {
                    var fetcher = $.ajax({
                            type    : "GET",
                            dataType: "json",
                            url     : contextPath + restPath + "?showAvatar=true&query=" + username
                        });

                    deferreds.push(fetcher);
                }
            });

            $.when.apply($, deferreds).done(function (i, j, k) {

                var user;
                function extractUser(resp) {
                    if ("users" in resp) {
                        user = resp.users[0];
                        usersFound.push(user.displayName);
                    }
                }

                if (arguments[0] instanceof Array) {
                    // multiple users
                    for(var arg = 0; arg < arguments.length; ++ arg) {
                        var arr = arguments[arg];
                        extractUser(arr[0]);
                    }
                }
                else {
                    // single user
                    extractUser(arguments[0]);
                }

                if (usersFound.length > 0) {
                    setDescription($field, AJS.I18n.getText('uph.current.value') + ": " + usersFound.join(", "));
                }
                else {
                    removeDescription($field);
                }
            });
        }
    }

    function createSingleUserPickers(ctx) {

        // cannot find a good way of finding multi-user picker fields - this is the best effort guess.
        // Otherwise needs a rest endpoint to identify those fields
        $()
            .add($("span.aui-iconfont-admin-roles", ctx).closest("div.ajax_autocomplete").find("textarea.long-field"))
            .add($(".userpickerfield", ctx))
            .each(function () {
                var $this = $(this);
                if ($this.data("aui-ss")) return;

                $this.on("change", function (e, user) {
                    setFieldDescription($this, $this.val());
                });

                // set the initial value - have to assume that in the case of
                // multiple users with similar names the first one is returned first
                setFieldDescription($this, $this.val());
            });
    }

    function removeDescription($field) {
        $field.parent().find("div.sr-user-displayname").remove();
    }

    function setDescription($field, desc) {

        removeDescription($field);
        var $parent = $field.parent();

        var $helpDiv = $parent.find("div.description");

            $helpDiv.before($("<div>", {
                "class": "sr-user-displayname",
                "style": "padding-top: 7px"
            }).text(desc));
    }

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
        if (reason !== JIRA.CONTENT_ADDED_REASON.panelRefreshed) {
            createSingleUserPickers(context);
        }
    });
})(AJS.$);



