# User Picker Helper

## Overview.

This plugin adds a description under single and multi user picker fields which shows the selected user(s) display names.

Before:

![Before](site/before.png)

After:

![After](site/after.png)


In short, it exists to partly solve [JRASERVER-31023](https://jira.atlassian.com/browse/JRASERVER-31023).

## Usage

Install. No configuration is required.

## Development


    mvn jira:debug

